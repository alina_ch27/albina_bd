﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private string ConnectionString = global::WindowsFormsApplication1.Properties.Settings.Default.Database1ConnectionString;
        public Form1()
        {
            InitializeComponent();
        }

        //private void search_TextChanged(object sender, EventArgs e)
        //{
        //    UpdateAbonentDGV();
        //    UpdateSprav();
        //}

        private void AppdateSQL()
        {
            SqlConnection sqlc = new SqlConnection();
            sqlc.ConnectionString = ConnectionString;
            sqlc.Open();
        }
        private string connectionstring = global::WindowsFormsApplication1.Properties.Settings.Default.Database1ConnectionString;
        private string abonentTableName = "artemicheva_abonent";
        private string contactTableName = "artemicheva_contact";
        private string providerTableName = "artemicheva_provider";

        void UpdateAbonentDGV()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var state = connection.State;
            var request = "SELECT * FROM [" + abonentTableName + "] WHERE surname + ' ' + name +' '+ patronymic LIKE '%" + search_fio.Text + "%'";//добавить фильтр по фио и номеру телефона
            var adapter = new SqlDataAdapter(request, connectionstring);
            var abonentTable = new DataTable();
            adapter.Fill(abonentTable);
            grid_abon.DataSource = abonentTable;
            grid_abon.Columns["id"].Visible = false;
            grid_abon.Columns["name"].HeaderText = "Имя";
            grid_abon.Columns["patronymic"].HeaderText = "Отчество";
            grid_abon.Columns["surname"].HeaderText = "Фамилия";
            grid_abon.Columns["address"].HeaderText = "Адрес";
            grid_abon.Columns["birth_date"].HeaderText = "День рождения";
            grid_abon.Columns["comment"].HeaderText = "Комментарий";
        }

        void UpdateContactDGV()
        {
            var request = "SELECT * From [" + contactTableName + "] WHERE phone + ' ' + type LIKE '%" + search_phone.Text + "%'";
            var adapter = new SqlDataAdapter(request, connectionstring);
            var contactTable = new DataTable();
            adapter.Fill(contactTable);
            grid_cont.DataSource = contactTable;
            grid_cont.Columns["id"].Visible = false;
            grid_cont.Columns["provider_id"].Visible = false;
            grid_cont.Columns["phone"].HeaderText = "Телефон";
            grid_cont.Columns["type"].HeaderText = "Тип номера";
        }
        void UpdateProvDGV()
        {
            var request = "SELECT * From artemicheva_provider";
            var adapter = new SqlDataAdapter(request, connectionstring);
            var provTable = new DataTable();
            adapter.Fill(provTable);
            grid_prov.DataSource = provTable;
            grid_prov.Columns["id"].Visible = false;
            grid_prov.Columns["name"].HeaderText = "Провайдер";
            grid_prov.Columns["score"].HeaderText = "Оценка";
        }
        void UpdateSprav()
        {

            var request = @"SELECT * FROM artemicheva_abonent RIGHT JOIN artemicheva_abonent_has_contact ON artemicheva_abonent.id=artemicheva_abonent_has_contact.abonent_id LEFT JOIN artemicheva_contact ON artemicheva_contact.id=artemicheva_abonent_has_contact.contact_id LEFT JOIN artemicheva_provider ON artemicheva_contact.provider_id=artemicheva_provider.id WHERE 1=1";

            if (search_phone.Text != "")
            {
                request += @" AND artemicheva_contact.phone LIKE '%" + search_phone.Text + "%'";

            }

            if (search_fio.Text != "")
            {
                request += @" AND artemicheva_abonent.surname+' '+artemicheva_abonent.patronymic+' '+artemicheva_abonent.name  LIKE+'%" + search_fio.Text + "%'";
            }

            var adapter = new SqlDataAdapter(request, connectionstring);
            var listtable = new DataTable();
            adapter.Fill(listtable);
            grid_phone.DataSource = listtable;
            grid_phone.Columns["id"].Visible = false;
            grid_phone.Columns["abonent_id"].Visible = false;
            grid_phone.Columns["contact_id"].Visible = false;
            grid_phone.Columns["id1"].Visible = false;
            grid_phone.Columns["id2"].Visible = false;
            grid_phone.Columns["name"].HeaderText = "Имя";
            grid_phone.Columns["surname"].HeaderText = "Фамилия";
            grid_phone.Columns["patronymic"].HeaderText = "Отчество";
            grid_phone.Columns["birth_date"].HeaderText = "Дата Рождения";
            grid_phone.Columns["comment"].Visible = false;
            grid_phone.Columns["address"].HeaderText = "Адрес";
            grid_phone.Columns["phone"].HeaderText = "Номер";
            grid_phone.Columns["type"].HeaderText = "Тип";
            grid_phone.Columns["provider_id"].Visible = false;
            grid_phone.Columns["name1"].HeaderText = "Провайдер";
            grid_phone.Columns["score"].Visible = false;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "database1DataSet.artemicheva_abonent_has_contact". При необходимости она может быть перемещена или удалена.
            this.artemicheva_abonent_has_contactTableAdapter.Fill(this.database1DataSet.artemicheva_abonent_has_contact);
            UpdateAbonentDGV();
            UpdateContactDGV();
            UpdateProvDGV();
            UpdateSprav();

        }

        private void search_phone_TextChanged(object sender, EventArgs e)
        {
            UpdateContactDGV();
            UpdateSprav();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            //var form = new Abform();

            ///* {
            //     var request = "SELECT *FROM Chugrina_provider";
            //     var adapter = new SqlDataAdapter(request, connectionstring);
            //     var orgtable = new DataTable();
            //     adapter.Fill(orgtable);
            //     var dict = new Dictionary<int, string>();
            //     foreach (DataRow row in orgtable.Rows)
            //     {
            //         dict.Add((int)row["id"], row["name"].ToString());
            //     }
            //     form.Organizationdata = dict;
            // }*/
            //var res = form.ShowDialog();
            //if (res == DialogResult.OK)
            //{
            //    var fname = form._Name.Text;
            //    var sname = form.sName.Text;
            //    var tname = form.tName.Text;
            //    var comment = form.Comment.Text;
            //    var addres = form.Address.Text;
            //    var birthday = form.Birthday.Text;
            //    var connection = new SqlConnection(ConnectionString);
            //    connection.Open();
            //    var reqiest = "INSERT INTO artemicheva_abonent (name,surname, patronymic, birth_date, comment, address) VALUES ('" + fname + "' ,'" + sname + "' ,'" + tname + "' ,'" + birthday + "' ,'" + comment + "' ,'" + addres + "')";
            //    var command = new SqlCommand(reqiest, connection);
            //    command.ExecuteNonQuery();
            //    connection.Close();
            //    UpdateAbonentDGV();
            //}
        }

        private void Change_Click(object sender, EventArgs e)
        {
            //var rov = grid_abon.SelectedRows.Count > 0 ? grid_abon.SelectedRows[0] : null;
            //if (rov == null)
            //{
            //    MessageBox.Show("Строчку выберите", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            //var form = new Abform();

            //form._Name.Text = rov.Cells["name"].Value.ToString();
            //form.sName.Text = rov.Cells["surname "].Value.ToString();
            //form.tName.Text = rov.Cells["patronymic"].Value.ToString();
            //form.Comment.Text = rov.Cells["comment"].Value.ToString();
            //form.Address.Text = rov.Cells["address"].Value.ToString();
            //form.Birthday.Text = rov.Cells["birth_date"].Value.ToString();
            //var res = form.ShowDialog();
            //if (res == DialogResult.OK)
            //{
            //    var fname = form._Name.Text;
            //    var sname = form.sName.Text;
            //    var tname = form.tName.Text;
            //    var comment = form.Comment.Text;
            //    var addres = form.Address.Text;
            //    var birthday = Convert.ToDateTime(form.Birthday.Text).ToString("yyyy-MM-dd HH:mm:ss.fff");
            //    var connection = new SqlConnection(ConnectionString);
            //    var id = rov.Cells["Id"].Value.ToString();
            //    //var orgId = form.OrgId;
            //    connection.Open();
            //    var reqiest = "UPDATE artemicheva_abonent SET name ='" + fname + "', surname ='" + sname + "', patronymic='"
            //        + tname + "', comment='" + comment + "', address='" + addres + "',birth_date = '" + birthday + "' WHERE Id=" + id + ";";
            //    var command = new SqlCommand(reqiest, connection);
            //    command.ExecuteNonQuery();
            //    connection.Close();
            //    UpdateAbonentDGV();
            //}
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            var rov = grid_abon.SelectedRows.Count > 0 ? grid_abon.SelectedRows[0] : null;
            if (rov == null)
            {
                MessageBox.Show("Выберите строчку ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var connection = new SqlConnection(ConnectionString);
            var id = rov.Cells["Id"].Value.ToString();
            connection.Open();
            var reqiest = "DELETE FROM artemicheva_abonent_has_contact  WHERE abonent_id=" + id + "DELETE FROM artemicheva_abonent WHERE Id=" + id;
            var command = new SqlCommand(reqiest, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateAbonentDGV();
        }

        private void Searchphone_Click(object sender, EventArgs e)
        {
            //UpdateContactDGV();
        }

        private void Add_pr_Click(object sender, EventArgs e)
        {
            //var form = new Prform();
            //var res = form.ShowDialog();
            //if (res == DialogResult.OK)
            //{
            //    var name = form.name_prov.Text;
            //    var score = form.score_prov.Text;

            //    var connection = new SqlConnection(ConnectionString);
            //    connection.Open();

            //    var reqiest = "INSERT INTO [" + providerTableName + "] (name,score) VALUES ('" + name + "' ,'" + score + "')";
            //    var command = new SqlCommand(reqiest, connection);
            //    command.ExecuteNonQuery();
            //    connection.Close();
            //    UpdateProvDGV();
            //}
        }

        private void Edit_pr_Click(object sender, EventArgs e)
        {
            //var rov = grid_prov.SelectedRows.Count > 0 ? grid_prov.SelectedRows[0] : null;
            //if (rov == null)
            //{
            //    MessageBox.Show("Выберите строчку ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            //var form = new Prform();

            //form.name_prov.Text = rov.Cells["name"].Value.ToString();
            //form.score_prov.Text = rov.Cells["score"].Value.ToString();
            //var res = form.ShowDialog();
            //if (res == DialogResult.OK)
            //{
            //    var namepr = form.name_prov.Text;
            //    var scorepr = form.score_prov.Text;
            //    var connection = new SqlConnection(ConnectionString);
            //    var id = rov.Cells["Id"].Value.ToString();
            //    //var orgId = form.OrgId;
            //    connection.Open();
            //    var reqiest = "UPDATE [" + providerTableName + "] SET name ='" + namepr + "', score ='" + scorepr;
            //    var command = new SqlCommand(reqiest, connection);
            //    command.ExecuteNonQuery();
            //    connection.Close();
            //    UpdateProvDGV();
            //}
        }
        private void Delete_pr_Click(object sender, EventArgs e)
        {
            var rov = grid_prov.SelectedRows.Count > 0 ? grid_prov.SelectedRows[0] : null;
            if (rov == null)
            {
                MessageBox.Show("Выберите строчку ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var connection = new SqlConnection(ConnectionString);
            var id = rov.Cells["Id"].Value.ToString();
            connection.Open();
            var reqiest = "DELETE FROM artemicheva_provider WHERE Id=" + id + ";";
            var command = new SqlCommand(reqiest, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateProvDGV();
            UpdateContactDGV();
            UpdateSprav();
            UpdateAbonentDGV();
        }

        private void Add_cont_Click(object sender, EventArgs e)
        {
            var form = new Contform();
            {
                var getReq = "SELECT *FROM artemicheva_provider";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                contactAdapter.Fill(providerTbl);

                foreach (DataRow row in providerTbl.Rows)
                {
                    dict.Add((int)row["Id"], row["name"].ToString());
                }
                form.ProviderData = dict;
            }

            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.ConForm_Phone_Number.Text;
                var type = form.ConForm_Type.Text;
                var provider_id = form.ProviderID;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO artemicheva_contact (phone, type, provider_id) VALUES ('" + phone + "', '" + type + "', '" + provider_id.ToString() + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateContactDGV();
            }
        }

        private void Edit_cont_Click(object sender, EventArgs e)
        {
            //var row = grid_cont.SelectedRows.Count > 0 ? grid_cont.SelectedRows[0] : null;
            //if (row == null)
            //{
            //    MessageBox.Show("Выберите строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            //var form = new Contform();
            //form.ConForm_Phone_Number.Text = row.Cells["phone"].Value.ToString();
            //form.ConForm_Type.Text = row.Cells["type"].Value.ToString();
            //{
            //    var getReq = "SELECT *FROM artemicheva_provider";
            //    var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
            //    var dict = new Dictionary<int, string>();
            //    var providerTbl = new DataTable();
            //    contactAdapter.Fill(providerTbl);

            //    foreach (DataRow dbrow in providerTbl.Rows)
            //    {
            //        dict.Add((int)dbrow["Id"], dbrow["name"].ToString());
            //    }
            //    form.ProviderData = dict;
            //}
            //form.ProviderID = (int)row.Cells["provider_id"].Value;
            //var res = form.ShowDialog();
            //if (res == DialogResult.OK)
            //{
            //    var phone = form.ConForm_Phone_Number.Text;
            //    var type = form.ConForm_Type.Text;
            //    var id = row.Cells["Id"].Value.ToString();
            //    var provider_id = form.ProviderID;
            //    var connection = new SqlConnection(ConnectionString);
            //    connection.Open();
            //    var request = "UPDATE artemicheva_contact SET phone = '" + phone + "', type = '" + type + "', provider_id=" + provider_id.ToString() +
            //         "WHERE Id = " + id + ";";
            //    var command = new SqlCommand(request, connection);
            //    command.ExecuteNonQuery();
            //    connection.Close();
            //    UpdateContactDGV();
            //}
        }

        private void Delete_cont_Click(object sender, EventArgs e)
        {
            //var row = grid_cont.SelectedRows.Count > 0 ? grid_cont.SelectedRows[0] : null;
            //if (row == null)
            //{
            //    MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            //var id = row.Cells["Id"].Value.ToString();
            //var connection = new SqlConnection(ConnectionString);
            //connection.Open();
            //var request = "DELETE FROM artemicheva_contact WHERE Id = " + id + ";";
            //var command = new SqlCommand(request, connection);
            //command.ExecuteNonQuery();
            //connection.Close();
            //UpdateContactDGV();
        }
        //cpravochnick
        private void Add_phone_to_fio_Click(object sender, EventArgs e)
        {
            //var form = new Phone_to_fio();

            //{
            //    var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + abonentTableName + "]";
            //    var Adapter = new SqlDataAdapter(getReq, ConnectionString);
            //    var dict = new Dictionary<int, string>();
            //    var Tbl = new DataTable();
            //    Adapter.Fill(Tbl);

            //    foreach (DataRow row in Tbl.Rows)
            //    {
            //        string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
            //        dict.Add((int)row["Id"], setS);
            //    }
            //    form.AbonentData = dict;
            //}

            //{
            //    var getReq = "SELECT Id, phone FROM" + "[" + contactTableName + "]";
            //    var Adapter = new SqlDataAdapter(getReq, ConnectionString);
            //    var dict = new Dictionary<int, string>();
            //    var Tbl = new DataTable();
            //    Adapter.Fill(Tbl);

            //    foreach (DataRow row in Tbl.Rows)
            //    {
            //        dict.Add((int)row["Id"], row["phone"].ToString());
            //    }
            //    form.ContactData = dict;
            //}

            //if (form.ShowDialog() == DialogResult.OK)
            //{
            //    var conn = new SqlConnection(ConnectionString);
            //    conn.Open();

            //    var request = "INSERT INTO artemicheva_abonent_has_contact" + "(abonent_id, contact_id)" + " VALUES " + "('" + form.AbonentID + "', '" + form.ContactID + "')";
            //    var com = new SqlCommand(request, conn);
            //    com.ExecuteNonQuery();

            //    conn.Close();

            //    UpdateSprav();
            //}
        }

        private void Edit_phone_to_fio_Click(object sender, EventArgs e)
        {
            //var row2 = grid_phone.SelectedRows.Count > 0 ? grid_phone.SelectedRows[0] : null;
            //if (row2 == null)
            //{
            //    MessageBox.Show("Выберите строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}

            //var form = new Phone_to_fio();
            //{
            //    var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + abonentTableName + "]";
            //    var Adapter = new SqlDataAdapter(getReq, ConnectionString);
            //    var dict = new Dictionary<int, string>();
            //    var Tbl = new DataTable();
            //    Adapter.Fill(Tbl);

            //    foreach (DataRow row in Tbl.Rows)
            //    {
            //        string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
            //        dict.Add((int)row["Id"], setS);
            //    }


            //    form.AbonentData = dict;
            //}

            //{
            //    var getReq = "SELECT Id, phone FROM" + "[" + contactTableName + "]";
            //    var Adapter = new SqlDataAdapter(getReq, ConnectionString);
            //    var dict = new Dictionary<int, string>();
            //    var Tbl = new DataTable();
            //    Adapter.Fill(Tbl);

            //    foreach (DataRow row in Tbl.Rows)
            //    {
            //        dict.Add((int)row["Id"], row["phone"].ToString());
            //    }


            //    form.ContactData = dict;
            //}
            //DataGridViewSelectedRowCollection Row = grid_phone.SelectedRows;

            //var mas = grid_phone.SelectedRows;
            //var Condidat = mas[0].Cells["Id"].FormattedValue.ToString();
            //var Condidat2 = mas[0].Cells["Id1"].FormattedValue.ToString();

            //var conn = new SqlConnection(ConnectionString);
            //conn.Open();
            //string str = "";
            //var req = "SELECT surname FROM artemicheva_abonent WHERE Id = " + Condidat + "";
            //var com = new SqlCommand(req, conn);
            //var last = com.ExecuteScalar();
            //str += last + " ";
            //req = "SELECT name FROM artemicheva_abonent WHERE Id = " + Condidat + "";
            //com = new SqlCommand(req, conn);
            //last = com.ExecuteScalar();
            //str += last + " ";
            //req = "SELECT patronymic FROM artemicheva_abonent WHERE Id = " + Condidat + "";
            //com = new SqlCommand(req, conn);
            //last = com.ExecuteScalar();
            //str += last;
            //form.FIO_PhDirecForm_ComBox.Text = str;

            //req = "SELECT phone FROM artemicheva_contact WHERE Id = " + Condidat2 + "";
            //com = new SqlCommand(req, conn);
            //last = com.ExecuteScalar();
            //form.Phone_PhDirecForm_ComBox.Text = last.ToString();

            //conn.Close();

            //if (form.ShowDialog() == DialogResult.OK)
            //{
            //    conn = new SqlConnection(ConnectionString);
            //    conn.Open();

            //    var request = "UPDATE artemicheva_abonent_has_contact " + " SET contact_id='" + form.ContactID +
            //        "', " + "abonent_id ='" + form.AbonentID + "' WHERE contact_id=" + Condidat2 +
            //        " AND abonent_id=" + Condidat;
            //    com = new SqlCommand(request, conn);
            //    com.ExecuteNonQuery();

            //    conn.Close();


            //    UpdateSprav();

            //}
        }

        private void Delete_phone_to_fio_Click(object sender, EventArgs e)
        {
            var row = grid_phone.SelectedRows.Count > 0 ? grid_phone.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var num1 = row.Cells["Id"].Value.ToString();
            var num2 = row.Cells["Id1"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM artemicheva_abonent_has_contact WHERE contact_id = '" + num2 +
                "' AND abonent_id='" + num1 + "'";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateSprav();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ////MessageBox.Show("Приветик");
            //SqlConnection conn = new SqlConnection();
            //conn.ConnectionString = global::WindowsFormsApplication1.Properties.Settings.Default.Database1ConnectionString;
            //conn.Open();
            //SqlCommand com = conn.CreateCommand();
            //com.CommandText = "SELECT * From artemicheva_abonent";
            //SqlDataReader reader = com.ExecuteReader();
            //string names = "";
            //while (reader.Read())
            //{
            //    names += reader["name"].ToString() + " ";
            //}
            //MessageBox.Show(names);
            //conn.Close();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            var row2 = grid_phone.SelectedRows.Count > 0 ? grid_phone.SelectedRows[0] : null;
            if (row2 == null)
            {
                MessageBox.Show("Выберите строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new Phone_to_fio();
            {
                var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + abonentTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }


                form.AbonentData = dict;
            }

            {
                var getReq = "SELECT Id, phone FROM" + "[" + contactTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }


                form.ContactData = dict;
            }
            DataGridViewSelectedRowCollection Row = grid_phone.SelectedRows;

            var mas = grid_phone.SelectedRows;
            var Condidat = mas[0].Cells["Id"].FormattedValue.ToString();
            var Condidat2 = mas[0].Cells["Id1"].FormattedValue.ToString();

            var conn = new SqlConnection(ConnectionString);
            conn.Open();
            string str = "";
            var req = "SELECT surname FROM artemicheva_abonent WHERE Id = " + Condidat + "";
            var com = new SqlCommand(req, conn);
            var last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT name FROM artemicheva_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT patronymic FROM artemicheva_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last;
            form.FIO_PhDirecForm_ComBox.Text = str;

            req = "SELECT phone FROM artemicheva_contact WHERE Id = " + Condidat2 + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            form.Phone_PhDirecForm_ComBox.Text = last.ToString();

            conn.Close();

            if (form.ShowDialog() == DialogResult.OK)
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "UPDATE artemicheva_abonent_has_contact " + " SET contact_id='" + form.ContactID +
                    "', " + "abonent_id ='" + form.AbonentID + "' WHERE contact_id=" + Condidat2 +
                    " AND abonent_id=" + Condidat;
                com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();

                conn.Close();


                UpdateSprav();

            }
        }
        private void button_add_click_Click(object sender, EventArgs e)
        {
            //var surname = "";
            //var name = "";
            //var patronymic = "";
            //var adress = "";
            //var comment = "";
            //var connection = new SqlConnection(connectionstring);
            //connection.Open();
            //var request = "INSERT INTO artemicheva_abonent (name, surname, patronymic, comment)";
        }
        private void grid_abon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


        private void Edit_cont_Click_1(object sender, EventArgs e)
        {
            var row = grid_cont.SelectedRows.Count > 0 ? grid_cont.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new Contform();
            form.ConForm_Phone_Number.Text = row.Cells["phone"].Value.ToString();
            form.ConForm_Type.Text = row.Cells["type"].Value.ToString();
            {
                var getReq = "SELECT *FROM artemicheva_provider";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                contactAdapter.Fill(providerTbl);

                foreach (DataRow dbrow in providerTbl.Rows)
                {
                    dict.Add((int)dbrow["Id"], dbrow["name"].ToString());
                }
                form.ProviderData = dict;
            }
            form.ProviderID = (int)row.Cells["provider_id"].Value;
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.ConForm_Phone_Number.Text;
                var type = form.ConForm_Type.Text;
                var id = row.Cells["Id"].Value.ToString();
                var provider_id = form.ProviderID;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE artemicheva_contact SET phone = '" + phone + "', type = '" + type + "', provider_id=" + provider_id.ToString() +
                     "WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateContactDGV();
            }
        }

        private void Delete_cont_Click_1(object sender, EventArgs e)
        {
            var row = grid_cont.SelectedRows.Count > 0 ? grid_cont.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM artemicheva_contact WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateContactDGV();
        }

        private void Add_pr_Click_1(object sender, EventArgs e)
        {
            var form = new Prform();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.name_prov.Text;
                var score = form.score_prov.Text;

                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var reqiest = "INSERT INTO [" + providerTableName + "] (name,score) VALUES ('" + name + "' ,'" + score + "')";
                var command = new SqlCommand(reqiest, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateProvDGV();
            }
        }

        private void Edit_pr_Click_1(object sender, EventArgs e)
        {
            var rov = grid_prov.SelectedRows.Count > 0 ? grid_prov.SelectedRows[0] : null;
            if (rov == null)
            {
                MessageBox.Show("Выберите строчку ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new Prform();

            form.name_prov.Text = rov.Cells["name"].Value.ToString();
            form.score_prov.Text = rov.Cells["score"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var namepr = form.name_prov.Text;
                var scorepr = form.score_prov.Text;
                var connection = new SqlConnection(ConnectionString);
                var id = rov.Cells["id"].Value.ToString();
                //var orgId = form.OrgId;
                connection.Open();
                var reqiest = "UPDATE [" + providerTableName + "] SET name ='" + namepr + "',score ='" + scorepr;
                var command = new SqlCommand(reqiest, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateProvDGV();
            }
        }

        private void Delete_pr_Click_1(object sender, EventArgs e)
        {
            var rov = grid_prov.SelectedRows.Count > 0 ? grid_prov.SelectedRows[0] : null;
            if (rov == null)
            {
                MessageBox.Show("Выберите строчку ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var connection = new SqlConnection(ConnectionString);
            var id = rov.Cells["Id"].Value.ToString();
            connection.Open();
            var reqiest = "DELETE FROM artemicheva_provider WHERE Id=" + id + ";";
            var command = new SqlCommand(reqiest, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateProvDGV();
            UpdateContactDGV();
            UpdateSprav();
            UpdateAbonentDGV();
        }

        private void Add_ab_Click(object sender, EventArgs e)
        {
            var form = new Abform();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var fname = form._Name.Text;
                var sname = form.sName.Text;
                var tname = form.tName.Text;
                var comment = form.Comment.Text;
                var addres = form.Address.Text;
                var birthday = form.Birthday.Text;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var reqiest = "INSERT INTO artemicheva_abonent (name,surname, patronymic, birth_date, comment, address) VALUES ('" + fname + "' ,'" + sname + "' ,'" + tname + "' ,'" + birthday + "' ,'" + comment + "' ,'" + addres + "')";
                var command = new SqlCommand(reqiest, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateAbonentDGV();
            }
        }

        private void Edit_ab_Click(object sender, EventArgs e)
        {
            var rov = grid_abon.SelectedRows.Count > 0 ? grid_abon.SelectedRows[0] : null;
            if (rov == null)
            {
                MessageBox.Show("Выберите cтрочку ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new Abform();

            form._Name.Text = rov.Cells["name"].Value.ToString();
            form.sName.Text = rov.Cells["surname"].Value.ToString();
            form.tName.Text = rov.Cells["patronymic"].Value.ToString();
            form.Comment.Text = rov.Cells["comment"].Value.ToString();
            form.Address.Text = rov.Cells["address"].Value.ToString();
            form.Birthday.Text = rov.Cells["birth_date"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var fname = form._Name.Text;
                var sname = form.sName.Text;
                var tname = form.tName.Text;
                var comment = form.Comment.Text;
                var addres = form.Address.Text;
                var birthday = Convert.ToDateTime(form.Birthday.Text).ToString("yyyy-MM-dd HH:mm:ss.fff");
                var connection = new SqlConnection(ConnectionString);
                var id = rov.Cells["Id"].Value.ToString();
                //var orgId = form.OrgId;
                connection.Open();
                var reqiest = "UPDATE artemicheva_abonent SET name ='" + fname + "', surname ='" + sname + "', patronymic='"
                    + tname + "', comment='" + comment + "', address='" + addres + "',birth_date = '" + birthday + "' WHERE Id=" + id + ";";
                var command = new SqlCommand(reqiest, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateAbonentDGV();
            }
        }

        private void Delete_ab_Click(object sender, EventArgs e)
        {
            var rov = grid_abon.SelectedRows.Count > 0 ? grid_abon.SelectedRows[0] : null;
            if (rov == null)
            {
                MessageBox.Show("Выберите строчку ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var connection = new SqlConnection(ConnectionString);
            var id = rov.Cells["Id"].Value.ToString();
            connection.Open();
            var reqiest = "DELETE FROM artemicheva_abonent_has_contact  WHERE abonent_id=" + id + "DELETE FROM artemicheva_abonent WHERE Id=" + id;
            var command = new SqlCommand(reqiest, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateAbonentDGV();
        }

        private void Add_phone_to_fio_Click_1(object sender, EventArgs e)
        {
            var form = new Phone_to_fio();

            {
                var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + abonentTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }
                form.AbonentData = dict;
            }

            {
                var getReq = "SELECT id, phone FROM" + "[" + contactTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["id"], row["phone"].ToString());
                }
                form.ContactData = dict;
            }

            if (form.ShowDialog() == DialogResult.OK)
            {
                var conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "INSERT INTO artemicheva_abonent_has_contact" + "(abonent_id, contact_id)" + " VALUES " + "('" + form.AbonentID + "', '" + form.ContactID + "')";
                var com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();

                conn.Close();

                UpdateSprav();
            }
        }

        private void Delete_phone_to_fio_Click_1(object sender, EventArgs e)
        {
            var row = grid_phone.SelectedRows.Count > 0 ? grid_phone.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var num1 = row.Cells["id"].Value.ToString();
            var num2 = row.Cells["id1"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM artemicheva_abonent_has_contact WHERE contact_id = '" + num2 +
                "' AND abonent_id='" + num1 + "'";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateSprav();
        }

        private void search_phone_TextChanged_1(object sender, EventArgs e)
        {
            UpdateContactDGV();
            UpdateSprav();
        }

        private void prov_Click(object sender, EventArgs e)
        {

        }

        private void search_fio_TextChanged(object sender, EventArgs e)
        {
            UpdateAbonentDGV();
        }
    }
}