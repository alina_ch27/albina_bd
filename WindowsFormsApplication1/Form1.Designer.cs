﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.TabSql = new System.Windows.Forms.TabControl();
            this.abon = new System.Windows.Forms.TabPage();
            this.Delete_ab = new System.Windows.Forms.Button();
            this.Edit_ab = new System.Windows.Forms.Button();
            this.Add_ab = new System.Windows.Forms.Button();
            this.grid_abon = new System.Windows.Forms.DataGridView();
            this.cont = new System.Windows.Forms.TabPage();
            this.Delete_cont = new System.Windows.Forms.Button();
            this.Edit_cont = new System.Windows.Forms.Button();
            this.Add_cont = new System.Windows.Forms.Button();
            this.grid_cont = new System.Windows.Forms.DataGridView();
            this.prov = new System.Windows.Forms.TabPage();
            this.Delete_pr = new System.Windows.Forms.Button();
            this.Edit_pr = new System.Windows.Forms.Button();
            this.Add_pr = new System.Windows.Forms.Button();
            this.grid_prov = new System.Windows.Forms.DataGridView();
            this.phone = new System.Windows.Forms.TabPage();
            this.Delete_phone_to_fio = new System.Windows.Forms.Button();
            this.Edit_phone_to_fio = new System.Windows.Forms.Button();
            this.Add_phone_to_fio = new System.Windows.Forms.Button();
            this.grid_phone = new System.Windows.Forms.DataGridView();
            this.search_phone = new System.Windows.Forms.TextBox();
            this.search_fio = new System.Windows.Forms.TextBox();
            this.database1DataSet = new WindowsFormsApplication1.Database1DataSet();
            this.database1DataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.artemichevaabonenthascontactBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.artemicheva_abonent_has_contactTableAdapter = new WindowsFormsApplication1.Database1DataSetTableAdapters.artemicheva_abonent_has_contactTableAdapter();
            this.button_add_click = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TabSql.SuspendLayout();
            this.abon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_abon)).BeginInit();
            this.cont.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_cont)).BeginInit();
            this.prov.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_prov)).BeginInit();
            this.phone.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_phone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.artemichevaabonenthascontactBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 605);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(10, 10);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TabSql
            // 
            this.TabSql.Controls.Add(this.abon);
            this.TabSql.Controls.Add(this.cont);
            this.TabSql.Controls.Add(this.prov);
            this.TabSql.Controls.Add(this.phone);
            this.TabSql.Location = new System.Drawing.Point(224, 2);
            this.TabSql.Name = "TabSql";
            this.TabSql.SelectedIndex = 0;
            this.TabSql.Size = new System.Drawing.Size(871, 548);
            this.TabSql.TabIndex = 1;
            // 
            // abon
            // 
            this.abon.BackColor = System.Drawing.Color.GhostWhite;
            this.abon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.abon.Controls.Add(this.Delete_ab);
            this.abon.Controls.Add(this.Edit_ab);
            this.abon.Controls.Add(this.Add_ab);
            this.abon.Controls.Add(this.grid_abon);
            this.abon.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.abon.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.abon.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.abon.Location = new System.Drawing.Point(4, 22);
            this.abon.Name = "abon";
            this.abon.Padding = new System.Windows.Forms.Padding(3);
            this.abon.Size = new System.Drawing.Size(863, 522);
            this.abon.TabIndex = 0;
            this.abon.Text = "Абонент";
            // 
            // Delete_ab
            // 
            this.Delete_ab.BackColor = System.Drawing.Color.SteelBlue;
            this.Delete_ab.ForeColor = System.Drawing.Color.White;
            this.Delete_ab.Location = new System.Drawing.Point(514, 491);
            this.Delete_ab.Name = "Delete_ab";
            this.Delete_ab.Size = new System.Drawing.Size(102, 23);
            this.Delete_ab.TabIndex = 3;
            this.Delete_ab.Text = "Удалить абонента";
            this.Delete_ab.UseVisualStyleBackColor = false;
            this.Delete_ab.Click += new System.EventHandler(this.Delete_ab_Click);
            // 
            // Edit_ab
            // 
            this.Edit_ab.BackColor = System.Drawing.Color.SteelBlue;
            this.Edit_ab.ForeColor = System.Drawing.Color.White;
            this.Edit_ab.Location = new System.Drawing.Point(397, 491);
            this.Edit_ab.Name = "Edit_ab";
            this.Edit_ab.Size = new System.Drawing.Size(111, 23);
            this.Edit_ab.TabIndex = 2;
            this.Edit_ab.Text = "Изменить абонента";
            this.Edit_ab.UseVisualStyleBackColor = false;
            this.Edit_ab.Click += new System.EventHandler(this.Edit_ab_Click);
            // 
            // Add_ab
            // 
            this.Add_ab.BackColor = System.Drawing.Color.SteelBlue;
            this.Add_ab.ForeColor = System.Drawing.Color.White;
            this.Add_ab.Location = new System.Drawing.Point(281, 491);
            this.Add_ab.Name = "Add_ab";
            this.Add_ab.Size = new System.Drawing.Size(110, 23);
            this.Add_ab.TabIndex = 1;
            this.Add_ab.Text = "Добавить абонента";
            this.Add_ab.UseVisualStyleBackColor = false;
            this.Add_ab.Click += new System.EventHandler(this.Add_ab_Click);
            // 
            // grid_abon
            // 
            this.grid_abon.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.grid_abon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_abon.Location = new System.Drawing.Point(-1, -1);
            this.grid_abon.Name = "grid_abon";
            this.grid_abon.Size = new System.Drawing.Size(863, 486);
            this.grid_abon.TabIndex = 0;
            this.grid_abon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_abon_CellContentClick);
            // 
            // cont
            // 
            this.cont.BackColor = System.Drawing.Color.GhostWhite;
            this.cont.Controls.Add(this.Delete_cont);
            this.cont.Controls.Add(this.Edit_cont);
            this.cont.Controls.Add(this.Add_cont);
            this.cont.Controls.Add(this.grid_cont);
            this.cont.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cont.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.cont.Location = new System.Drawing.Point(4, 22);
            this.cont.Name = "cont";
            this.cont.Padding = new System.Windows.Forms.Padding(3);
            this.cont.Size = new System.Drawing.Size(863, 522);
            this.cont.TabIndex = 1;
            this.cont.Text = "Контакт";
            // 
            // Delete_cont
            // 
            this.Delete_cont.BackColor = System.Drawing.Color.SteelBlue;
            this.Delete_cont.ForeColor = System.Drawing.Color.White;
            this.Delete_cont.Location = new System.Drawing.Point(515, 493);
            this.Delete_cont.Name = "Delete_cont";
            this.Delete_cont.Size = new System.Drawing.Size(99, 23);
            this.Delete_cont.TabIndex = 3;
            this.Delete_cont.Text = "Удалить контакт";
            this.Delete_cont.UseVisualStyleBackColor = false;
            this.Delete_cont.Click += new System.EventHandler(this.Delete_cont_Click_1);
            // 
            // Edit_cont
            // 
            this.Edit_cont.BackColor = System.Drawing.Color.SteelBlue;
            this.Edit_cont.ForeColor = System.Drawing.Color.White;
            this.Edit_cont.Location = new System.Drawing.Point(393, 493);
            this.Edit_cont.Name = "Edit_cont";
            this.Edit_cont.Size = new System.Drawing.Size(116, 23);
            this.Edit_cont.TabIndex = 2;
            this.Edit_cont.Text = "Изменить контакт";
            this.Edit_cont.UseVisualStyleBackColor = false;
            this.Edit_cont.Click += new System.EventHandler(this.Edit_cont_Click_1);
            // 
            // Add_cont
            // 
            this.Add_cont.BackColor = System.Drawing.Color.SteelBlue;
            this.Add_cont.ForeColor = System.Drawing.Color.White;
            this.Add_cont.Location = new System.Drawing.Point(279, 493);
            this.Add_cont.Name = "Add_cont";
            this.Add_cont.Size = new System.Drawing.Size(108, 23);
            this.Add_cont.TabIndex = 1;
            this.Add_cont.Text = "Добавить контакт";
            this.Add_cont.UseVisualStyleBackColor = false;
            this.Add_cont.Click += new System.EventHandler(this.Add_cont_Click);
            // 
            // grid_cont
            // 
            this.grid_cont.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.grid_cont.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_cont.GridColor = System.Drawing.Color.Teal;
            this.grid_cont.Location = new System.Drawing.Point(0, 0);
            this.grid_cont.Name = "grid_cont";
            this.grid_cont.Size = new System.Drawing.Size(1282, 487);
            this.grid_cont.TabIndex = 0;
            // 
            // prov
            // 
            this.prov.BackColor = System.Drawing.Color.GhostWhite;
            this.prov.Controls.Add(this.Delete_pr);
            this.prov.Controls.Add(this.Edit_pr);
            this.prov.Controls.Add(this.Add_pr);
            this.prov.Controls.Add(this.grid_prov);
            this.prov.Location = new System.Drawing.Point(4, 22);
            this.prov.Name = "prov";
            this.prov.Size = new System.Drawing.Size(863, 522);
            this.prov.TabIndex = 2;
            this.prov.Text = "Провайдеры";
            this.prov.Click += new System.EventHandler(this.prov_Click);
            // 
            // Delete_pr
            // 
            this.Delete_pr.BackColor = System.Drawing.Color.SteelBlue;
            this.Delete_pr.ForeColor = System.Drawing.Color.White;
            this.Delete_pr.Location = new System.Drawing.Point(548, 496);
            this.Delete_pr.Name = "Delete_pr";
            this.Delete_pr.Size = new System.Drawing.Size(122, 23);
            this.Delete_pr.TabIndex = 3;
            this.Delete_pr.Text = "Удалить провайдера";
            this.Delete_pr.UseVisualStyleBackColor = false;
            this.Delete_pr.Click += new System.EventHandler(this.Delete_pr_Click_1);
            // 
            // Edit_pr
            // 
            this.Edit_pr.BackColor = System.Drawing.Color.SteelBlue;
            this.Edit_pr.ForeColor = System.Drawing.Color.White;
            this.Edit_pr.Location = new System.Drawing.Point(411, 496);
            this.Edit_pr.Name = "Edit_pr";
            this.Edit_pr.Size = new System.Drawing.Size(131, 23);
            this.Edit_pr.TabIndex = 2;
            this.Edit_pr.Text = "Изменить провайдера";
            this.Edit_pr.UseVisualStyleBackColor = false;
            this.Edit_pr.Click += new System.EventHandler(this.Edit_pr_Click_1);
            // 
            // Add_pr
            // 
            this.Add_pr.BackColor = System.Drawing.Color.SteelBlue;
            this.Add_pr.ForeColor = System.Drawing.Color.White;
            this.Add_pr.Location = new System.Drawing.Point(275, 496);
            this.Add_pr.Name = "Add_pr";
            this.Add_pr.Size = new System.Drawing.Size(130, 23);
            this.Add_pr.TabIndex = 1;
            this.Add_pr.Text = "Добавить провайдера";
            this.Add_pr.UseVisualStyleBackColor = false;
            this.Add_pr.Click += new System.EventHandler(this.Add_pr_Click_1);
            // 
            // grid_prov
            // 
            this.grid_prov.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.grid_prov.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_prov.Location = new System.Drawing.Point(0, 0);
            this.grid_prov.Name = "grid_prov";
            this.grid_prov.Size = new System.Drawing.Size(1282, 490);
            this.grid_prov.TabIndex = 0;
            // 
            // phone
            // 
            this.phone.Controls.Add(this.Delete_phone_to_fio);
            this.phone.Controls.Add(this.Edit_phone_to_fio);
            this.phone.Controls.Add(this.Add_phone_to_fio);
            this.phone.Controls.Add(this.grid_phone);
            this.phone.Location = new System.Drawing.Point(4, 22);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(863, 522);
            this.phone.TabIndex = 3;
            this.phone.Text = "Тел. справочник";
            this.phone.UseVisualStyleBackColor = true;
            // 
            // Delete_phone_to_fio
            // 
            this.Delete_phone_to_fio.BackColor = System.Drawing.Color.SteelBlue;
            this.Delete_phone_to_fio.ForeColor = System.Drawing.Color.White;
            this.Delete_phone_to_fio.Location = new System.Drawing.Point(486, 496);
            this.Delete_phone_to_fio.Name = "Delete_phone_to_fio";
            this.Delete_phone_to_fio.Size = new System.Drawing.Size(75, 23);
            this.Delete_phone_to_fio.TabIndex = 3;
            this.Delete_phone_to_fio.Text = "Удалить";
            this.Delete_phone_to_fio.UseVisualStyleBackColor = false;
            this.Delete_phone_to_fio.Click += new System.EventHandler(this.Delete_phone_to_fio_Click_1);
            // 
            // Edit_phone_to_fio
            // 
            this.Edit_phone_to_fio.BackColor = System.Drawing.Color.SteelBlue;
            this.Edit_phone_to_fio.ForeColor = System.Drawing.Color.White;
            this.Edit_phone_to_fio.Location = new System.Drawing.Point(405, 496);
            this.Edit_phone_to_fio.Name = "Edit_phone_to_fio";
            this.Edit_phone_to_fio.Size = new System.Drawing.Size(75, 23);
            this.Edit_phone_to_fio.TabIndex = 2;
            this.Edit_phone_to_fio.Text = "Изменить";
            this.Edit_phone_to_fio.UseVisualStyleBackColor = false;
            this.Edit_phone_to_fio.Click += new System.EventHandler(this.button3_Click);
            // 
            // Add_phone_to_fio
            // 
            this.Add_phone_to_fio.BackColor = System.Drawing.Color.SteelBlue;
            this.Add_phone_to_fio.ForeColor = System.Drawing.Color.White;
            this.Add_phone_to_fio.Location = new System.Drawing.Point(324, 496);
            this.Add_phone_to_fio.Name = "Add_phone_to_fio";
            this.Add_phone_to_fio.Size = new System.Drawing.Size(75, 23);
            this.Add_phone_to_fio.TabIndex = 1;
            this.Add_phone_to_fio.Text = "Добавить";
            this.Add_phone_to_fio.UseVisualStyleBackColor = false;
            this.Add_phone_to_fio.Click += new System.EventHandler(this.Add_phone_to_fio_Click_1);
            // 
            // grid_phone
            // 
            this.grid_phone.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.grid_phone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_phone.Location = new System.Drawing.Point(0, 0);
            this.grid_phone.Name = "grid_phone";
            this.grid_phone.Size = new System.Drawing.Size(1270, 490);
            this.grid_phone.TabIndex = 0;
            // 
            // search_phone
            // 
            this.search_phone.BackColor = System.Drawing.Color.AliceBlue;
            this.search_phone.Location = new System.Drawing.Point(621, 556);
            this.search_phone.Name = "search_phone";
            this.search_phone.Size = new System.Drawing.Size(100, 20);
            this.search_phone.TabIndex = 2;
            this.search_phone.TextChanged += new System.EventHandler(this.search_phone_TextChanged_1);
            // 
            // search_fio
            // 
            this.search_fio.BackColor = System.Drawing.Color.AliceBlue;
            this.search_fio.Location = new System.Drawing.Point(621, 582);
            this.search_fio.Name = "search_fio";
            this.search_fio.Size = new System.Drawing.Size(100, 20);
            this.search_fio.TabIndex = 3;
            this.search_fio.TextChanged += new System.EventHandler(this.search_fio_TextChanged);
            // 
            // database1DataSet
            // 
            this.database1DataSet.DataSetName = "Database1DataSet";
            this.database1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // database1DataSetBindingSource
            // 
            this.database1DataSetBindingSource.DataSource = this.database1DataSet;
            this.database1DataSetBindingSource.Position = 0;
            // 
            // artemichevaabonenthascontactBindingSource
            // 
            this.artemichevaabonenthascontactBindingSource.DataMember = "artemicheva_abonent_has_contact";
            this.artemichevaabonenthascontactBindingSource.DataSource = this.database1DataSetBindingSource;
            // 
            // artemicheva_abonent_has_contactTableAdapter
            // 
            this.artemicheva_abonent_has_contactTableAdapter.ClearBeforeFill = true;
            // 
            // button_add_click
            // 
            this.button_add_click.BackColor = System.Drawing.SystemColors.Highlight;
            this.button_add_click.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button_add_click.Location = new System.Drawing.Point(4, 605);
            this.button_add_click.Name = "button_add_click";
            this.button_add_click.Size = new System.Drawing.Size(10, 10);
            this.button_add_click.TabIndex = 4;
            this.button_add_click.Text = "button2";
            this.button_add_click.UseVisualStyleBackColor = false;
            this.button_add_click.Click += new System.EventHandler(this.button_add_click_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Location = new System.Drawing.Point(727, 559);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Поиск по телефону";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Location = new System.Drawing.Point(727, 585);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Поиск по имени";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1297, 614);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_add_click);
            this.Controls.Add(this.search_fio);
            this.Controls.Add(this.search_phone);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TabSql);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.TabSql.ResumeLayout(false);
            this.abon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid_abon)).EndInit();
            this.cont.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid_cont)).EndInit();
            this.prov.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid_prov)).EndInit();
            this.phone.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid_phone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.artemichevaabonenthascontactBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl TabSql;
        private System.Windows.Forms.TabPage abon;
        private System.Windows.Forms.TabPage cont;
        private System.Windows.Forms.TabPage prov;
        private System.Windows.Forms.TabPage phone;
        private System.Windows.Forms.DataGridView grid_abon;
        private System.Windows.Forms.DataGridView grid_cont;
        private System.Windows.Forms.DataGridView grid_prov;
        private System.Windows.Forms.DataGridView grid_phone;
        private System.Windows.Forms.TextBox search_phone;
        private System.Windows.Forms.TextBox search_fio;
        private System.Windows.Forms.BindingSource database1DataSetBindingSource;
        private Database1DataSet database1DataSet;
        private System.Windows.Forms.BindingSource artemichevaabonenthascontactBindingSource;
        private Database1DataSetTableAdapters.artemicheva_abonent_has_contactTableAdapter artemicheva_abonent_has_contactTableAdapter;
        private System.Windows.Forms.Button button_add_click;
        private System.Windows.Forms.Button Delete_ab;
        private System.Windows.Forms.Button Edit_ab;
        private System.Windows.Forms.Button Add_ab;
        private System.Windows.Forms.Button Delete_cont;
        private System.Windows.Forms.Button Edit_cont;
        private System.Windows.Forms.Button Add_cont;
        private System.Windows.Forms.Button Delete_pr;
        private System.Windows.Forms.Button Edit_pr;
        private System.Windows.Forms.Button Add_pr;
        private System.Windows.Forms.Button Delete_phone_to_fio;
        private System.Windows.Forms.Button Edit_phone_to_fio;
        private System.Windows.Forms.Button Add_phone_to_fio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

