﻿namespace WindowsFormsApplication1
{
    partial class Contform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConForm_Phone_Number = new System.Windows.Forms.TextBox();
            this.ConForm_Type = new System.Windows.Forms.TextBox();
            this.ConForm_ComBox_Prov = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AbFom_OK_Button = new System.Windows.Forms.Button();
            this.AbFom_CANCEL_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ConForm_Phone_Number
            // 
            this.ConForm_Phone_Number.Location = new System.Drawing.Point(12, 7);
            this.ConForm_Phone_Number.Name = "ConForm_Phone_Number";
            this.ConForm_Phone_Number.Size = new System.Drawing.Size(120, 20);
            this.ConForm_Phone_Number.TabIndex = 0;
            // 
            // ConForm_Type
            // 
            this.ConForm_Type.Location = new System.Drawing.Point(12, 33);
            this.ConForm_Type.Name = "ConForm_Type";
            this.ConForm_Type.Size = new System.Drawing.Size(120, 20);
            this.ConForm_Type.TabIndex = 1;
            // 
            // ConForm_ComBox_Prov
            // 
            this.ConForm_ComBox_Prov.FormattingEnabled = true;
            this.ConForm_ComBox_Prov.Location = new System.Drawing.Point(12, 59);
            this.ConForm_ComBox_Prov.Name = "ConForm_ComBox_Prov";
            this.ConForm_ComBox_Prov.Size = new System.Drawing.Size(120, 21);
            this.ConForm_ComBox_Prov.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(164, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(138, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Тип телефона";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(138, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Провайдер";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(138, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Номер телефона";
            // 
            // AbFom_OK_Button
            // 
            this.AbFom_OK_Button.Location = new System.Drawing.Point(12, 101);
            this.AbFom_OK_Button.Name = "AbFom_OK_Button";
            this.AbFom_OK_Button.Size = new System.Drawing.Size(75, 23);
            this.AbFom_OK_Button.TabIndex = 7;
            this.AbFom_OK_Button.Text = "Добавить";
            this.AbFom_OK_Button.UseVisualStyleBackColor = true;
            this.AbFom_OK_Button.Click += new System.EventHandler(this.AbFom_OK_Button_Click);
            // 
            // AbFom_CANCEL_Button
            // 
            this.AbFom_CANCEL_Button.Location = new System.Drawing.Point(141, 101);
            this.AbFom_CANCEL_Button.Name = "AbFom_CANCEL_Button";
            this.AbFom_CANCEL_Button.Size = new System.Drawing.Size(75, 23);
            this.AbFom_CANCEL_Button.TabIndex = 8;
            this.AbFom_CANCEL_Button.Text = "Отмена";
            this.AbFom_CANCEL_Button.UseVisualStyleBackColor = true;
            this.AbFom_CANCEL_Button.Click += new System.EventHandler(this.AbFom_CANCEL_Button_Click);
            // 
            // Contform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 132);
            this.Controls.Add(this.AbFom_CANCEL_Button);
            this.Controls.Add(this.AbFom_OK_Button);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ConForm_ComBox_Prov);
            this.Controls.Add(this.ConForm_Type);
            this.Controls.Add(this.ConForm_Phone_Number);
            this.Name = "Contform";
            this.Text = "Контакт";
            this.Load += new System.EventHandler(this.Contform_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox ConForm_Phone_Number;
        public System.Windows.Forms.TextBox ConForm_Type;
        public System.Windows.Forms.ComboBox ConForm_ComBox_Prov;
        public System.Windows.Forms.Button AbFom_OK_Button;
        public System.Windows.Forms.Button AbFom_CANCEL_Button;
    }
}